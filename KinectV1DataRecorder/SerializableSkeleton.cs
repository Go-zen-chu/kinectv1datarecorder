﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Windows;

namespace KinectV1DataRecorder
{
    [DataContract]
    public class SerializableSkeleton
    {
        [DataMember]
        public Skeleton Skeleton { get; set; }
        [DataMember]
        //     Gets or sets the mapped skeleton joints.
        public Dictionary<JointType, Point> MappedJoints { get; set; }

        public SerializableSkeleton() { }
    }
}
