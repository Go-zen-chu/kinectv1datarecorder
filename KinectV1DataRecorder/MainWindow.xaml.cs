﻿using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace KinectV1DataRecorder
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        // INotifyPropertyChangedPropertyChanged event to allow window controls to bind to changeable data
        public event PropertyChangedEventHandler PropertyChanged;

        KinectSensorChooser kinectChooser;

        // Size of the RGB pixel in the bitmap
        readonly int bytesPerPixel = (PixelFormats.Bgr32.BitsPerPixel + 7) / 8;
        public const ushort KINECT_VALID_MAX_DEPTH = 10000;
        // Lool up table for converting depth to color
        readonly byte[] lut_g = new byte[KINECT_VALID_MAX_DEPTH];
        readonly byte[] lut_b = new byte[KINECT_VALID_MAX_DEPTH];

        #region Recording related variables
        bool isRecording = false;
        DateTime prevRecordedTime = DateTime.MinValue;

        string colorImagesSavingDirPath;
        string depthImagesSavingDirPath;
        string skeletonDataSavingDirPath;
        #endregion

        // for calculating FPS
        private DateTime prevRenderedTime = DateTime.MinValue;

        #region Color related variables
        ColorImageFormat lastColorImageFormat = ColorImageFormat.Undefined;
        byte[] colorPixels;
        #endregion

        #region Depth related variables
        DepthImageFormat lastDepthImageFormat = DepthImageFormat.Undefined;
        DepthImagePixel[] depthFrameData;
        ColorImagePoint[] mappedDepthPoints;
        short[] mappedDepthData = null;
        byte[] depthPixels;
        #endregion

        #region Properties
        private WriteableBitmap colorBitmap = null;
        public ImageSource ColorImageSource
        {
            get
            {
                return this.colorBitmap;
            }
        }

        private WriteableBitmap depthBitmap = null;
        public ImageSource DepthImageSource
        {
            get
            {
                return this.depthBitmap;
            }
        }

        int recordingFps = 500; // = 500ms
        public int RecordingFPS
        {
            get { return recordingFps; }
            set { recordingFps = value; }
        }

        private int kinect_fps = 0;
        public int FPS
        {
            get { return kinect_fps; }
            set
            {
                if (this.kinect_fps != value)
                {
                    this.kinect_fps = value;
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("FPS"));
                    }
                }
            }
        }

        private string statusText = null;
        // Gets or sets the current status text to display
        public string StatusText
        {
            get
            {
                return this.statusText;
            }

            set
            {
                if (this.statusText != value)
                {
                    this.statusText = value;

                    // notify any bound elements that the text has changed
                    if (this.PropertyChanged != null)
                    {
                        this.PropertyChanged(this, new PropertyChangedEventArgs("StatusText"));
                    }
                }
            }
        }
        #endregion

        // CONSTRUCTER
        public MainWindow()
        {
            kinectChooser = new KinectSensorChooser();
            kinectChooser.KinectChanged += KinectStateChanged;
            kinectChooser.PropertyChanged += KinectPropertyChanged;
            try
            {
                kinectChooser.Start();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, ex.GetType().ToString());
            }

            // set the status text
            this.StatusText = kinectChooser.Status.ToString();
            // use the window object as the view model in this simple example
            this.DataContext = this;
            // initialize the components (controls) of the window
            this.InitializeComponent();

            if (Directory.Exists(Properties.Settings.Default.MainWindow_RecordedDataRootPath) == false)
                startRecordingToggleButton.IsEnabled = false;

            // Create LUT
            for (ushort depth = 0; depth < KINECT_VALID_MAX_DEPTH; depth++)
            {
                lut_g[depth] = (byte)(depth / 256);
                lut_b[depth] = (byte)(depth % 256);
            }
        }
        /// <summary>
        /// Execute shutdown tasks
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void MainWindow_Closing(object sender, CancelEventArgs e){
            try
            {
                if (kinectChooser != null) kinectChooser.Stop();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message, ex.GetType().ToString());
            }
        }

        #region kinectのセットアップ
        void KinectStateChanged(object sender, KinectChangedEventArgs e)
        {
            if (e.OldSensor != null) FinishKinectSensor(e.OldSensor);
            if (e.NewSensor != null) InitKinectSensor(e.NewSensor);
        }
        void KinectPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if ("Status" == e.PropertyName) this.StatusText = kinectChooser.Status.ToString();
        }
        void InitKinectSensor(KinectSensor kinect)
        {
            kinect.ColorStream.Enable(ColorImageFormat.RgbResolution640x480Fps30);
            this.colorBitmap = new WriteableBitmap(kinect.ColorStream.FrameWidth, kinect.ColorStream.FrameHeight, 96, 96, PixelFormats.Bgr32, null);
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("ColorImageSource"));

            kinect.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
            this.depthBitmap = new WriteableBitmap(kinect.DepthStream.FrameWidth, kinect.DepthStream.FrameHeight, 96, 96, PixelFormats.Bgra32, null);
            if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("DepthImageSource"));

            kinect.SkeletonStream.Enable();

            kinect.AllFramesReady += SensorAllFrameReady;
        }
        void FinishKinectSensor(KinectSensor kinect)
        {
            kinect.Dispose();
        }
        #endregion

        // Actual measurement
        private void SensorAllFrameReady(object sender, AllFramesReadyEventArgs e)
        {
            var kinect = sender as KinectSensor;
            int colorWidth = 0, colorHeight = 0;
            int depthWidth = 0, depthHeight = 0;

            bool colorFrameProcessed = false;
            bool depthFrameProcessed = false;
            bool skeletonFrameProcessed = false;

            #region Prepare color image
            using (var colorFrame = e.OpenColorImageFrame())
            {
                if (colorFrame == null) return;
                colorWidth = colorFrame.Width;
                colorHeight = colorFrame.Height;
                bool cHaveNewFormat = this.lastColorImageFormat != colorFrame.Format;
                if (cHaveNewFormat)
                {
                    this.lastColorImageFormat = colorFrame.Format;
                    this.colorPixels = new byte[colorFrame.PixelDataLength];
                    colorBitmap = new WriteableBitmap(colorWidth, colorHeight, 96, 96, PixelFormats.Bgr32, null);
                    if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("ColorImageSource"));
                }
                // Copy the pixel data from the image to a temporary array
                colorFrame.CopyPixelDataTo(this.colorPixels);
                this.Dispatcher.Invoke(new Action(() =>
                {
                    this.colorBitmap.WritePixels(new Int32Rect(0, 0, colorWidth, colorHeight), this.colorPixels, colorWidth * sizeof(int), 0);
                }));
                colorFrameProcessed = true;
            }
            #endregion

            #region Obtain Depth data
            using (var depthFrame = e.OpenDepthImageFrame())
            {
                if(depthFrame ==null) return;
                depthWidth = depthFrame.Width;
                depthHeight = depthFrame.Height;
                var dHaveNewFormat = this.lastDepthImageFormat != depthFrame.Format;
                if (dHaveNewFormat)
                {
                    this.lastDepthImageFormat = depthFrame.Format;
                    this.depthFrameData = new DepthImagePixel[depthFrame.PixelDataLength];
                    // 深さ情報に色を合わせるため、ここでは深さ情報を組み入れる
                    this.mappedDepthPoints = new ColorImagePoint[depthFrame.PixelDataLength];
                    this.mappedDepthData = new short[colorWidth * colorHeight];
                    depthBitmap = new WriteableBitmap(depthWidth, depthHeight, 96, 96, PixelFormats.Bgra32, null);
                    if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("DepthImageSource"));
                }
                depthFrame.CopyDepthImagePixelDataTo(this.depthFrameData);
                depthFrameProcessed = true;
            }
            #endregion

            #region Prepare Skeleton

            Skeleton[] skeletons = new Skeleton[0];
            using (var skeletonFrame = e.OpenSkeletonFrame())
            {
                if (skeletonFrame != null)
                {
                    skeletons = new Skeleton[skeletonFrame.SkeletonArrayLength];
                    skeletonFrame.CopySkeletonDataTo(skeletons);
                    skeletonFrameProcessed = true;
                }
            } 
            #endregion

            // calculate fps
            var currentTime = DateTime.Now;
            this.FPS = (int)(1000 / (currentTime - prevRenderedTime).TotalMilliseconds);
            prevRenderedTime = currentTime;

            #region Prepare Depth image
            if (colorFrameProcessed && depthFrameProcessed)
            {
                this.depthPixels = new byte[depthWidth * depthHeight * bytesPerPixel];
                // 深さ画像のあるピクセルが、実画像のどのピクセルと一致しているのかをmappedDepthPointsに格納する
                // mappedDepthPoints[0].X は深さ画像の一番最初のピクセルと一致する実画像のピクセルのX座標
                // depthFrame の (0,0) が (39,40) にマッピングされたりする
                kinect.CoordinateMapper.MapDepthFrameToColorFrame(lastDepthImageFormat, depthFrameData, lastColorImageFormat, mappedDepthPoints);
                Parallel.For(0, depthHeight, row =>
                {
                    int depthIdx = depthWidth * row;
                    for (int col = 0; col < depthWidth; ++col)
                    {
                        var mappedPoint = this.mappedDepthPoints[depthIdx];
                        mappedDepthData[depthIdx] = 0;
                        if (!float.IsNegativeInfinity(mappedPoint.X) && !float.IsNegativeInfinity(mappedPoint.Y))
                        {
                            // make sure the depth pixel maps to a valid point in color space
                            int mappedX = (int)Math.Floor(mappedPoint.X + 0.5f);
                            int mappedY = (int)Math.Floor(mappedPoint.Y + 0.5f);
                            if ((mappedX >= 0) && (mappedX < colorWidth) && (mappedY >= 0) && (mappedY < colorHeight))
                            {
                                int mappedIndex = ((mappedY * colorWidth) + mappedX) * bytesPerPixel; // point on color image
                                var depth = depthFrameData[depthIdx].Depth;
                                // insert color image mapped depth value
                                mappedDepthData[depthIdx] = depth;
                                if (0 <= depth && depth <= KINECT_VALID_MAX_DEPTH)
                                {
                                    this.depthPixels[mappedIndex++] = lut_b[depth]; // b
                                    this.depthPixels[mappedIndex++] = lut_g[depth]; // g
                                    this.depthPixels[mappedIndex++] = 0; // r
                                    this.depthPixels[mappedIndex++] = 0xff; // a
                                }
                            }
                        }
                        depthIdx++;
                    }
                });
                this.depthBitmap.WritePixels(new Int32Rect(0, 0, depthWidth, depthHeight), this.depthPixels, depthWidth * bytesPerPixel, 0);
            }
            #endregion

            #region Map Skeleton to Color Image
            var mappedJointPosDicts = new Dictionary<int, Dictionary<JointType, Point>>();
            if (skeletonFrameProcessed)
            {
                if (skeletons.Length >= 0) // usually the length is 6 (fix value)
                {
                    skeletonCanvas.Children.Clear();
                    foreach (var sk in skeletons)
                    {
                        if (sk.TrackingState == SkeletonTrackingState.Tracked)
                        {
                            var mappedJointPosDict = MapSkeletonToScreen(kinect.CoordinateMapper, sk);
                            DrawBonesAndJoints(mappedJointPosDict, skeletonCanvas);
                            mappedJointPosDicts.Add(sk.TrackingId, mappedJointPosDict);
                        }
                    }
                }
            }
            
            #endregion

            if (isRecording && (currentTime - prevRecordedTime).TotalMilliseconds > recordingFps)
            {
                #region Save data
                prevRecordedTime = currentTime;
                var currentTimeStr = currentTime.ToString(Properties.Resources.DateTimeFormatText);

                if (colorFrameProcessed && showColorCheckBox.IsChecked.Value)
                {
                    // Saving Color image Data
                    var encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(this.colorBitmap));
                    var colorImgPath = Path.Combine(colorImagesSavingDirPath, currentTimeStr + ".png");
                    using (var fs = new FileStream(colorImgPath, FileMode.Create)) encoder.Save(fs);
                }

                if (depthFrameProcessed && showDepthCheckBox.IsChecked.Value)
                {
                    // Saving Depth Data
                    var encoder = new PngBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(this.depthBitmap));
                    var depthImgPath = Path.Combine(depthImagesSavingDirPath, currentTimeStr + ".png");
                    using (var fs = new FileStream(depthImgPath, FileMode.Create)) encoder.Save(fs);
                }

                if (skeletonFrameProcessed && showSkeletonCheckBox.IsChecked.Value)
                {
                    if (skeletons.Length >= 0) // usually the length is 6 (fix value)
                    {
                        var skeletonDataDirPath = Path.Combine(skeletonDataSavingDirPath, currentTimeStr);
                        foreach (var sk in skeletons)
                        {
                            if (sk.TrackingState == SkeletonTrackingState.Tracked)
                            {
                                Directory.CreateDirectory(skeletonDataDirPath);
                                var serializableSk = new SerializableSkeleton() { Skeleton = sk, MappedJoints = mappedJointPosDicts[sk.TrackingId] };
                                ExportDataJson<SerializableSkeleton>(Path.Combine(skeletonDataDirPath, sk.TrackingId + ".json"), serializableSk);
                            }
                        }
                    } 
                }
                #endregion
            }
        }

        private Dictionary<JointType, Point> MapSkeletonToScreen(CoordinateMapper coordinateMapper, Skeleton skeleton)
        {
            var mappedJointPosDict = new Dictionary<JointType, Point>();
            foreach (var joint in skeleton.Joints.ToArray())
            {
                if (joint.TrackingState != JointTrackingState.NotTracked)
                {
                    var colorPoint = coordinateMapper.MapSkeletonPointToColorPoint(joint.Position, ColorImageFormat.RgbResolution640x480Fps30);
                    mappedJointPosDict.Add(joint.JointType, new Point(colorPoint.X, colorPoint.Y));
                }
            }
            return mappedJointPosDict;
        }

        private void DrawBonesAndJoints(Dictionary<JointType, Point> mappedJointPosDict, Canvas canvas)
        {
            // Render Torso
            DrawBone(mappedJointPosDict, canvas, JointType.Head, JointType.ShoulderCenter);
            DrawBone(mappedJointPosDict, canvas, JointType.ShoulderCenter, JointType.ShoulderLeft);
            DrawBone(mappedJointPosDict, canvas, JointType.ShoulderCenter, JointType.ShoulderRight);
            DrawBone(mappedJointPosDict, canvas, JointType.ShoulderCenter, JointType.Spine);
            DrawBone(mappedJointPosDict, canvas, JointType.Spine, JointType.HipCenter);
            DrawBone(mappedJointPosDict, canvas, JointType.HipCenter, JointType.HipLeft);
            DrawBone(mappedJointPosDict, canvas, JointType.HipCenter, JointType.HipRight);

            // Left Arm
            DrawBone(mappedJointPosDict, canvas, JointType.ShoulderLeft, JointType.ElbowLeft);
            DrawBone(mappedJointPosDict, canvas, JointType.ElbowLeft, JointType.WristLeft);
            DrawBone(mappedJointPosDict, canvas, JointType.WristLeft, JointType.HandLeft);

            // Right Arm
            DrawBone(mappedJointPosDict, canvas, JointType.ShoulderRight, JointType.ElbowRight);
            DrawBone(mappedJointPosDict, canvas, JointType.ElbowRight, JointType.WristRight);
            DrawBone(mappedJointPosDict, canvas, JointType.WristRight, JointType.HandRight);

            // Left Leg
            DrawBone(mappedJointPosDict, canvas, JointType.HipLeft, JointType.KneeLeft);
            DrawBone(mappedJointPosDict, canvas, JointType.KneeLeft, JointType.AnkleLeft);
            DrawBone(mappedJointPosDict, canvas, JointType.AnkleLeft, JointType.FootLeft);

            // Right Leg
            DrawBone(mappedJointPosDict, canvas, JointType.HipRight, JointType.KneeRight);
            DrawBone(mappedJointPosDict, canvas, JointType.KneeRight, JointType.AnkleRight);
            DrawBone(mappedJointPosDict, canvas, JointType.AnkleRight, JointType.FootRight);
        }

        private void DrawBone(Dictionary<JointType, Point> mappedJointPosDict, Canvas canvas, JointType jointType0, JointType jointType1, Brush brush = null, double thickness = 3)
        {
            if (mappedJointPosDict.ContainsKey(jointType0) == false || mappedJointPosDict.ContainsKey(jointType1) == false) return;
            var p0 = mappedJointPosDict[jointType0];
            var p1 = mappedJointPosDict[jointType1];
            if(brush == null) brush = Brushes.LightGreen;
            var line = new System.Windows.Shapes.Line() { Stroke = brush, StrokeThickness = thickness, X1 = p0.X, Y1 = p0.Y, X2 = p1.X, Y2 = p1.Y };
            canvas.Children.Add(line);
        }


        private void showImagesCheckBox_Click(object sender, RoutedEventArgs e)
        {
            var checkbox = sender as System.Windows.Controls.CheckBox;
            if (checkbox == null) return;
            FrameworkElement targetImageOrCanvas = null;
            if (checkbox.Name == showColorCheckBox.Name)
                targetImageOrCanvas = colorImage;
            else if (checkbox.Name == showDepthCheckBox.Name)
                targetImageOrCanvas = depthImage;
            else if (checkbox.Name == showSkeletonCheckBox.Name)
                targetImageOrCanvas = skeletonCanvas;

            if (targetImageOrCanvas == null) return;
            if (checkbox.IsChecked.Value)
                targetImageOrCanvas.Visibility = System.Windows.Visibility.Visible;
            else
                targetImageOrCanvas.Visibility = System.Windows.Visibility.Hidden;
        }

        private void dataViewButton_Click(object sender, RoutedEventArgs e)
        {
            var dataViewWindow = new DataViewWindow(Properties.Settings.Default.MainWindow_RecordedDataRootPath);
            dataViewWindow.Show();
        }

        /// <summary>
        /// Handles the user clicking on the screenshot button
        /// </summary>
        /// <param name="sender">object sending the event</param>
        /// <param name="e">event arguments</param>
        private void startRecordingToggleButton_Click(object sender, RoutedEventArgs e)
        {
            if (startRecordingToggleButton == null) return; // GUI is not initialized yet
            if (startRecordingToggleButton.IsChecked.Value) // Start Recording
            {
                isRecording = true;
                startRecordingToggleButton.Content = Properties.Resources.StopRecordingDataText;
                startRecordingToggleButton.Background = Brushes.Coral;

                var startRecordingSessionTime = DateTime.Now; // Time when recording has started
                var appRootPath = Properties.Settings.Default.MainWindow_RecordedDataRootPath;
                var startRecordingSessionTimeStr = startRecordingSessionTime.ToString(Properties.Resources.DateTimeFormatText);
                colorImagesSavingDirPath = Path.Combine(appRootPath, Properties.Resources.DirColorImages, startRecordingSessionTimeStr);
                depthImagesSavingDirPath = Path.Combine(appRootPath, Properties.Resources.DirDepthImages, startRecordingSessionTimeStr);
                skeletonDataSavingDirPath = Path.Combine(appRootPath, Properties.Resources.DirSkeleton, startRecordingSessionTimeStr);
                Directory.CreateDirectory(colorImagesSavingDirPath);
                Directory.CreateDirectory(depthImagesSavingDirPath);
                Directory.CreateDirectory(skeletonDataSavingDirPath);
                recordingFPSTextBox.IsEnabled = false;
            }
            else // End Recording
            {
                isRecording = false;
                startRecordingToggleButton.Content = Properties.Resources.StartRecordingDataText;
                startRecordingToggleButton.Background = Brushes.LightGreen;
                recordingFPSTextBox.IsEnabled = true;
            }
        }

        private void selectRecordedDataExportPathButton_Click(object sender, RoutedEventArgs e)
        {
            var fbd = new FolderBrowserDialog();
            fbd.Description = "Choose Recorded Data Export Path";
            if (Directory.Exists(Properties.Settings.Default.MainWindow_RecordedDataRootPath) == false)
            {
                fbd.SelectedPath = Properties.Settings.Default.MainWindow_RecordedDataRootPath;
            }
            if (fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (Directory.Exists(fbd.SelectedPath) == false) return;
                recordedDataExportPathTextBox.Text = fbd.SelectedPath;
                Properties.Settings.Default.MainWindow_RecordedDataRootPath = fbd.SelectedPath;
                Properties.Settings.Default.Save();
                startRecordingToggleButton.IsEnabled = true;
            }
        }


        public static void ExportDataJson<T>(string exportPath, T obj)
        {
            var serializer = new DataContractJsonSerializer(typeof(T));
            using (var fs = new FileStream(exportPath, FileMode.Create, FileAccess.Write))
                serializer.WriteObject(fs, obj);
        }
        public static T ImportJsonData<T>(string importPath)
        {
            var serializer = new DataContractJsonSerializer(typeof(T));
            using (var fs = new FileStream(importPath, FileMode.Open, FileAccess.Read))
                return (T)serializer.ReadObject(fs);
        }
    
    }
}
